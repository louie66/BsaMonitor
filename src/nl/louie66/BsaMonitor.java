package nl.louie66;

import java.util.Scanner;

public class BsaMonitor {
    /*
    Programma om te bepalen of the BSA positief of
    negatief uitvalt aan de hand van behaalde cijfers
    @author Rene de Leeuw
     */

    public static void main(String[] args) {
        // De namen van vakken/ projecten in constanten opslaan
        final String VAK1 = "Project Fasten Your Seatbelts";
        final String VAK2 = "Programming";
        final String VAK3 = "Databases";
        final String VAK4 = "Personal Skills";
        final String VAK5 = "Project Skills";
        final String VAK6 = "Infrastructure";
        final String VAK7 = "Network Engineering 1";

        // Dit zijn de punten te behalen per vak als de cijfers boven een 5.5 zijn
        int succesCijferVak1 = 12;
        int succesCijferVak2 = 3;
        int succesCijferVak3 = 3;
        int succesCijferVak4 = 2;
        int succesCijferVak5 = 2;
        int succesCijferVak6 = 3;
        int succesCijferVak7 = 3;

        // het totaal opslaan hierna veranderen de succesCijferVak[n] variabelen
        int totaalSuccesCijfer = succesCijferVak1 + succesCijferVak2 +
                succesCijferVak3 + succesCijferVak4 + succesCijferVak5 +
                succesCijferVak6 + succesCijferVak7;

        // Bekijken welke naam het langst is (nummer is nodig voor uitlijning)
        int maxLength = 0;
        maxLength = VAK1.length() > maxLength ? VAK1.length() : maxLength;
        maxLength = VAK2.length() > maxLength ? VAK2.length() : maxLength;
        maxLength = VAK3.length() > maxLength ? VAK3.length() : maxLength;
        maxLength = VAK4.length() > maxLength ? VAK4.length() : maxLength;
        maxLength = VAK5.length() > maxLength ? VAK5.length() : maxLength;
        maxLength = VAK6.length() > maxLength ? VAK6.length() : maxLength;
        maxLength = VAK7.length() > maxLength ? VAK7.length() : maxLength;

        // input Scanner object

        Scanner input = new Scanner(System.in);
        System.out.println("Voer behaalde cijfers in:");

        // De cijfers opslaan en meteen het succesCijfer aanpassen indien nodig
        System.out.print(VAK1 + ": ");
        double cijferVak1 = input.nextDouble();
        succesCijferVak1 = cijferVak1 >= 5.5 ? succesCijferVak1 : 0;
        System.out.print(VAK2 + ": ");
        double cijferVak2 = input.nextDouble();
        succesCijferVak2 = cijferVak2 >= 5.5 ? succesCijferVak2 : 0;
        System.out.print(VAK3 + ": ");
        double cijferVak3 = input.nextDouble();
        succesCijferVak3 = cijferVak3 >= 5.5 ? succesCijferVak3 : 0;
        System.out.print(VAK4 + ": ");
        double cijferVak4 = input.nextDouble();
        succesCijferVak4 = cijferVak4 >= 5.5 ? succesCijferVak4 : 0;
        System.out.print(VAK5 + ": ");
        double cijferVak5 = input.nextDouble();
        succesCijferVak5 = cijferVak5 >= 5.5 ? succesCijferVak5 : 0;
        System.out.print(VAK6 + ": ");
        double cijferVak6 = input.nextDouble();
        succesCijferVak6 = cijferVak6 >= 5.5 ? succesCijferVak6 : 0;
        System.out.print(VAK7 + ": ");
        double cijferVak7 = input.nextDouble();
        succesCijferVak7 = cijferVak7 >= 5.5 ? succesCijferVak7 : 0;
        System.out.println();

        // de behaalde studiepunten ophalen
        int puntenTotaal = succesCijferVak1 + succesCijferVak2 +
                succesCijferVak3 + succesCijferVak4 + succesCijferVak5 +
                succesCijferVak6 + succesCijferVak7;

        // Output
        String vakBuilder = rightpad(VAK1, maxLength);
        System.out.printf("Vak/project: %s Cijfer: %s Behaalde punten: %s\n",
                vakBuilder, cijferVak1, succesCijferVak1);
        vakBuilder = rightpad(VAK2, maxLength);
        System.out.printf("Vak/project: %s Cijfer: %s Behaalde punten: %s\n",
                vakBuilder, cijferVak2, succesCijferVak2);
        vakBuilder = rightpad(VAK3, maxLength);
        System.out.printf("Vak/project: %s Cijfer: %s Behaalde punten: %s\n",
                vakBuilder, cijferVak3, succesCijferVak3);
        vakBuilder = rightpad(VAK4, maxLength);
        System.out.printf("Vak/project: %s Cijfer: %s Behaalde punten: %s\n",
                vakBuilder, cijferVak4, succesCijferVak4);
        vakBuilder = rightpad(VAK5, maxLength);
        System.out.printf("Vak/project: %s Cijfer: %s Behaalde punten: %s\n",
                vakBuilder, cijferVak5, succesCijferVak5);
        vakBuilder = rightpad(VAK6, maxLength);
        System.out.printf("Vak/project: %s Cijfer: %s Behaalde punten: %s\n",
                vakBuilder, cijferVak6, succesCijferVak6);
        vakBuilder = rightpad(VAK7, maxLength);
        System.out.printf("Vak/project: %s Cijfer: %s Behaalde punten: %s\n",
                vakBuilder, cijferVak7, succesCijferVak7);
        System.out.println();

        System.out.printf("Totaal behaalde studiepunten: %s/%s\n", puntenTotaal,
                totaalSuccesCijfer);

        // Zonodig een waarschuwing laten zien bij een negatieve BSA
        if (puntenTotaal < ((totaalSuccesCijfer * 5) / 6)) {
            System.out.print("PAS OP: je ligt op schema voor een negatief BSA!");
        }
    }

    // Static method om spaties uit te vullen
    private static String rightpad(String text, int length) {
        // return String.format("%-8.8s", "Vak1");  print "Vak1" + 4 spaties (8 tekens)
        return String.format("%-" + length + "." + length + "s", text);
    }
}
